#!/bin/bash

cd ../python_scripts

nohup python3 002_Generate_pairs_mm_and_dv.py 0 50      > ../log_files/np_px_000-050_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 50 100    > ../log_files/np_px_050-100_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 100 150   > ../log_files/np_px_100-150_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 150 200   > ../log_files/np_px_150-200_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 200 250   > ../log_files/np_px_200-250_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 250 300   > ../log_files/np_px_250-300_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 300 350   > ../log_files/np_px_300-350_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 350 400   > ../log_files/np_px_350-400_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 400 450   > ../log_files/np_px_400-450_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 450 500   > ../log_files/np_px_450-500_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 500 550   > ../log_files/np_px_500-550_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 550 600   > ../log_files/np_px_550-600_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 600 650   > ../log_files/np_px_600-650_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 650 700   > ../log_files/np_px_650-700_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 700 750   > ../log_files/np_px_700-750_z1.log &
nohup python3 002_Generate_pairs_mm_and_dv.py 750 767   > ../log_files/np_px_750-767_z1.log &
