#!/bin/bash

cd ../python_scripts

nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 0   50  > ../log_files/bkg_agn_px_000-050_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 50  100 > ../log_files/bkg_agn_px_050-100_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 100 150 > ../log_files/bkg_agn_px_100-150_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 150 200 > ../log_files/bkg_agn_px_150-200_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 200 250 > ../log_files/bkg_agn_px_200-250_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 250 300 > ../log_files/bkg_agn_px_250-300_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 300 350 > ../log_files/bkg_agn_px_300-350_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 350 400 > ../log_files/bkg_agn_px_350-400_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 400 450 > ../log_files/bkg_agn_px_400-450_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 450 500 > ../log_files/bkg_agn_px_450-500_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 500 550 > ../log_files/bkg_agn_px_500-550_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 550 600 > ../log_files/bkg_agn_px_550-600_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 600 650 > ../log_files/bkg_agn_px_600-650_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 650 700 > ../log_files/bkg_agn_px_650-700_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 700 750 > ../log_files/bkg_agn_px_700-750_z2.log &
nohup python3 011_Estimating_AGN_background_flux_per_pixel.py 750 767 > ../log_files/bkg_agn_px_750-767_z2.log &