#!/bin/bash

cd ../python_scripts

nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 0 50    0.2 3  > ../log_files/sr_Lx_M500c_px_000-050_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 50 100  0.2 3  > ../log_files/sr_Lx_M500c_px_050-100_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 100 150 0.2 3  > ../log_files/sr_Lx_M500c_px_100-150_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 150 200 0.2 3  > ../log_files/sr_Lx_M500c_px_150-200_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 200 250 0.2 3  > ../log_files/sr_Lx_M500c_px_200-250_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 250 300 0.2 3  > ../log_files/sr_Lx_M500c_px_250-300_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 300 350 0.2 3  > ../log_files/sr_Lx_M500c_px_300-350_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 350 400 0.2 3  > ../log_files/sr_Lx_M500c_px_350-400_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 400 450 0.2 3  > ../log_files/sr_Lx_M500c_px_400-450_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 450 500 0.2 3  > ../log_files/sr_Lx_M500c_px_450-500_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 500 550 0.2 3  > ../log_files/sr_Lx_M500c_px_500-550_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 550 600 0.2 3  > ../log_files/sr_Lx_M500c_px_550-600_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 600 650 0.2 3  > ../log_files/sr_Lx_M500c_px_600-650_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 650 700 0.2 3  > ../log_files/sr_Lx_M500c_px_650-700_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 700 750 0.2 3  > ../log_files/sr_Lx_M500c_px_700-750_cp0.2_modelA3.log &
nohup python3 009_get_M500c_Lx_RF_relations_all_sky.py 750 767 0.2 3  > ../log_files/sr_Lx_M500c_px_750-767_cp0.2_modelA3.log &
