#!/bin/bash

cd ../python_scripts

nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 0 10 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_0-10.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 10 100 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_10-100.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 100 200 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_100-200.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 200 300 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_200-300.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 300 400 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_300-400.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 400 500 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_400-500.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 500 600 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_500-600.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 600 700 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_600-700.log &
nohup python3 007_Generating_pairs_mm_dv_tmm_and_xoff.py 700 767 0.17 0.54 > ../log_files/pairs_xoff_0.17-0.54_tmm_Nil-Nil_px_700-767.log &